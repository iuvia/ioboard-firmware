// (C) 2020 David Pello Gonzalez for IUVIA.io
// WiFi related functions

#ifndef __IUVIAWIFI_H
#define __IUVIAWIFI_H

// Includes
#include <inttypes.h>
#include <ESP8266WiFi.h>
// definition of pins, serial protocol, etc
#include "iuviaioboard.h"


int wifi_scan(int n, char* buffer, unsigned int buflen);
int wifi_connect(char* ssid, uint8_t* ip);
int wifi_connect_password(char* ssid, char* password, uint8_t* ip);
int wifi_change_mac(uint8_t* mac);


#endif
