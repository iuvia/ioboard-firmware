// (C) 2020 David Pello Gonzalez for IUVIA.io

#ifndef __IUVIAIOBOARD_H
#define __IUVIAIOBOARD_H

#include <inttypes.h>

// board configuration
#define LEDPIN            D4
#define NUMPIXELS         8
#define INTERNAL_LED      LED_BUILTIN

// function return error types
#define PARAM_ERROR       -1
#define SCAN_ERROR        -5
#define BUFFER_ERROR      -3


// LED interface

typedef struct {
  uint8_t R, G, B;
} RgbColor;

#define IUVIA_COLOR_R     0
#define IUVIA_COLOR_G     100
#define IUVIA_COLOR_B     100
#define ERROR_COLOR_R     200
#define ERROR_COLOR_G     0
#define ERROR_COLOR_B     0

// Serial protocol
#define SERIAL_SPEED      115200
#define BUFFER_SIZE       512
#define SEPARATOR         '\n'

// packet types
#define PKT_CMD           0xCC
#define PKT_STAT          0x57
#define PKT_DATA          0xDA

// commands
#define CMD_LED_DELAY     0x1D
#define CMD_LED_CLR       0x1C
#define CMD_LED_FILL      0x1F
#define CMD_LED_FADEIN    0x12
#define CMD_LED_FADEOUT   0x13
#define CMD_LED_BREATH    0x1B
#define CMD_LED_RAINBW    0x19
#define CMD_LED_IUVIA     0x11
#define CMD_LED_TRAIL     0x17
#define CMD_LED_ERROR     0x1E

#define CMD_PWR_GET       0x96

#define CMD_WIFI_SCAN     0x35
#define CMD_WIFI_CONN     0x3C
#define CMD_WIFI_CONN_PSK 0x39
#define CMD_WIFI_CHG_MAC  0x33

// data
#define DATA_DEBUG        0xDD
#define DATA_PWR          0xD9
#define DATA_SSIDS        0xD5
#define DATA_CONN         0xDC

// status
#define STAT_OK           0x14
#define STAT_ERR          0xEE
#define STAT_INIT         0x17

// status errors
#define ERR_LEDS          0xE1  // problem with LEDs
#define ERR_PARAMS        0xE2  // wrong function parameters
#define ERR_BUFFER        0xE3  // buffer size exceeded
#define ERR_SCAN          0xE5  // problem scanning WiFis
#define ERR_CONN          0xE6  // problem connecting to WiFi
#define ERR_WRONG         0xEE  // wrong command
#define ERR_UNKN          0xEF  // unknown error

// IPs
#define IPV4              0x04
#define IPV6              0x06

#endif
