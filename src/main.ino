// (C) 2018 David Pello Gonzalez for IUVIA.io
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.
// If not, see <http://www.gnu.org/licenses/>.


// Includes
#include <ESP8266WiFi.h>
// WS2812FX Library
#include <WS2812FX.h>
// PacketSerial https://github.com/bakercp/PacketSerial
#include <PacketSerial.h>
// definition of pins, serial protocol, etc
#include "iuviaioboard.h"
// wifi related functions
#include "wifi.h"

extern "C" {
#include <user_interface.h>
}

// variables
uint8_t ssids_packet[BUFFER_SIZE]; // buffer for WiFi scans

// create objects
WS2812FX pixels = WS2812FX(NUMPIXELS, LEDPIN, NEO_GRB + NEO_KHZ800);
PacketSerial ioPacketSerial;


// Functions

// Internal LED blink, debug purposes
void board_blink() {
    // inverted logic
    digitalWrite(INTERNAL_LED, LOW);
    delay(20);
    digitalWrite(INTERNAL_LED, HIGH);
    delay(20);
}
         

//////// PacketSerial callback /////////
void on_packet_received(const uint8_t* buffer, size_t size)
{
    uint8_t stat_ok_packet[2] = { PKT_STAT, STAT_OK };
    uint8_t stat_err_packet[2] = { PKT_STAT, STAT_ERR };
    // Process your decoded incoming packet here.
    switch (buffer[0]) {
        case PKT_CMD:
            // command packet
            parse_command(buffer, size);
            break;
        case PKT_STAT:
            // status packet, just send OK
            ioPacketSerial.send(stat_ok_packet, 2);
            break;
        default:
            // unknown packet type
            ioPacketSerial.send(stat_err_packet, 2);
            break;
    }

}

// Parse and execute received commands
void parse_command(const uint8_t* buffer, size_t size)
{
    // prepare status packets, we'll need them
    uint8_t stat_ok_packet[2] = { PKT_STAT, STAT_OK };

    // we know the packet is a commmand, test the second byte (if there is one)
    if (size > 1) {
        switch (buffer[1]) {
            case CMD_LED_DELAY:
                // we need two parameters
                if (size > 3) {
                    ioPacketSerial.send(stat_ok_packet, 2);
                    pixels.setSpeed((buffer[2] << 8) + buffer[3]);
                } else {
                    // error
                    uint8_t stat_err_packet[3] = { PKT_STAT, STAT_ERR, ERR_PARAMS };
                    ioPacketSerial.send(stat_err_packet, 3);
                }
                break;
            case CMD_LED_CLR:
                // clear LEDs
                pixels.setColor(0);
                pixels.setMode(FX_MODE_STATIC);
                ioPacketSerial.send(stat_ok_packet, 2);
                break;
            case CMD_LED_FILL:
                // we need three parameters
                if (size > 4) {
                    ioPacketSerial.send(stat_ok_packet, 2);
                    pixels.setColor(buffer[2], buffer[3], buffer[4]);
                    pixels.setMode(FX_MODE_STATIC);
                } else {
                    // error
                    uint8_t stat_err_packet[3] = { PKT_STAT, STAT_ERR, ERR_PARAMS };
                    ioPacketSerial.send(stat_err_packet, 3);
                }
                break;
            case CMD_LED_FADEIN:
                // we need at least two parameters
                if (size > 2) {
                    // if RGB specified
                    if (size >= 5) {
                        ioPacketSerial.send(stat_ok_packet, 2);
                        //led_fadein(buffer[2], buffer[3], buffer[4], buffer[5]);
                    } else {
                        ioPacketSerial.send(stat_ok_packet, 2);
                        //led_fadein(buffer[2], color.R, color.G, color.B);
                    }
                } else {
                    // error
                    uint8_t stat_err_packet[3] = { PKT_STAT, STAT_ERR, ERR_PARAMS };
                    ioPacketSerial.send(stat_err_packet, 3);
                }
                break;
            case CMD_LED_FADEOUT:
                // we need at least two parameters
                if (size > 2) {
                    // if RGB specified
                    if (size >= 5) {
                        ioPacketSerial.send(stat_ok_packet, 2);
                        //led_fadeout(buffer[2], buffer[3], buffer[4], buffer[5]);
                    } else {
                        ioPacketSerial.send(stat_ok_packet, 2);
                        //led_fadeout(buffer[2], color.R, color.G, color.B);
                    }
                } else {
                    // error
                    uint8_t stat_err_packet[3] = { PKT_STAT, STAT_ERR, ERR_PARAMS };
                    ioPacketSerial.send(stat_err_packet, 3);
                }
                break;
            case CMD_LED_BREATH:
                // check parameters
                // if RGB specified
                if (size > 4) {
                    ioPacketSerial.send(stat_ok_packet, 2);
                    pixels.setColor(buffer[2], buffer[3], buffer[4]);
                    pixels.setMode(FX_MODE_FADE);
                } else {
                    ioPacketSerial.send(stat_ok_packet, 2);
                    pixels.setColor(IUVIA_COLOR_R, IUVIA_COLOR_G, IUVIA_COLOR_B);
                    pixels.setMode(FX_MODE_FADE);
                }
                break;
            case CMD_LED_RAINBW:
                ioPacketSerial.send(stat_ok_packet, 2);
                pixels.setMode(FX_MODE_RAINBOW_CYCLE);
                break;
            case CMD_LED_IUVIA:
                // if RGB specified
                if (size > 4) {
                    ioPacketSerial.send(stat_ok_packet, 2);
                    pixels.setColor(buffer[2], buffer[3], buffer[4]);
                    pixels.setMode(FX_MODE_FIRE_FLICKER);
                } else {
                    ioPacketSerial.send(stat_ok_packet, 2);
                    pixels.setColor(IUVIA_COLOR_R, IUVIA_COLOR_G, IUVIA_COLOR_B);
                    pixels.setMode(FX_MODE_FIRE_FLICKER);
                }
                break;
            case CMD_LED_TRAIL:
                // if RGB specified
                if (size > 4) {
                    ioPacketSerial.send(stat_ok_packet, 2);
                    pixels.setColor(buffer[2], buffer[3], buffer[4]);
                    pixels.setMode(FX_MODE_LARSON_SCANNER);
                } else {
                    ioPacketSerial.send(stat_ok_packet, 2);
                    pixels.setColor(IUVIA_COLOR_R, IUVIA_COLOR_G, IUVIA_COLOR_B);
                    pixels.setMode(FX_MODE_LARSON_SCANNER);
                }
                break;
            case CMD_LED_ERROR:
                // show error
                ioPacketSerial.send(stat_ok_packet, 2);
                pixels.setColor(ERROR_COLOR_R, ERROR_COLOR_G, ERROR_COLOR_B);
                pixels.setMode(FX_MODE_STATIC);
                break;
            case CMD_WIFI_SCAN:
                // we need one parameter
                if (size > 2) {
                    // scan n netwoks (buffer[2]), result at packet+2
                    int ssids_len = wifi_scan(buffer[2], (char *) ssids_packet+2, BUFFER_SIZE-2);
                    if (ssids_len > 0) {
                        ssids_packet[0] = PKT_DATA;
                        ssids_packet[1] = DATA_SSIDS;
                        ioPacketSerial.send(ssids_packet, ssids_len+2);
                    } else {
                        // check errors
                        if (ssids_len == 0) {
                            uint8_t stat_err_packet[3] = { PKT_STAT, STAT_ERR, ERR_SCAN };
                            ioPacketSerial.send(stat_err_packet, 3);
                        } else if (ssids_len == BUFFER_ERROR) {
                            uint8_t stat_err_packet[3] = { PKT_STAT, STAT_ERR, ERR_BUFFER };
                            ioPacketSerial.send(stat_err_packet, 3);
                        } else if (ssids_len == PARAM_ERROR) {
                            uint8_t stat_err_packet[3] = { PKT_STAT, STAT_ERR, ERR_PARAMS };
                            ioPacketSerial.send(stat_err_packet, 3);
                        } else {
                            uint8_t stat_err_packet[3] = { PKT_STAT, STAT_ERR, ERR_UNKN };
                            ioPacketSerial.send(stat_err_packet, 3);
                        }
                    }
                } else {
                    // error
                    uint8_t stat_err_packet[3] = { PKT_STAT, STAT_ERR, ERR_PARAMS };
                    ioPacketSerial.send(stat_err_packet, 3);
                }    
                break;
            case CMD_WIFI_CONN:
                // we need data parameters
                if (size > 2) {
                    // we need to get ssid from packet
                    char ssid[33];

                    // ssid, starts at buffer+2
                    unsigned int i;
                    for (i=0; i<size-2; i++) {
                        ssid[i] = buffer[i+2];
                    }
                    ssid[i] = '\0';

                    // try to connect
                    uint8_t ip[4];
                    int connected = wifi_connect(ssid, ip);
                    if (connected == EXIT_SUCCESS) {
                        // ok, connected
                        uint8_t data_conn_packet[7] = { PKT_DATA, DATA_CONN, IPV4, ip[0], ip[1], ip[2], ip[3] };
                        ioPacketSerial.send(data_conn_packet, 7);
                        WiFi.disconnect();
                    } else {
                        // error
                        uint8_t stat_err_packet[3] = { PKT_STAT, STAT_ERR, ERR_CONN };
                        ioPacketSerial.send(stat_err_packet, 3);
                    }
                } else {
                    // error
                    uint8_t stat_err_packet[3] = { PKT_STAT, STAT_ERR, ERR_PARAMS };
                    ioPacketSerial.send(stat_err_packet, 3);
                }
                break;
            case CMD_WIFI_CONN_PSK:
                // we need data parameters
                if (size > 2) {
                    // we need to get ssid and password from packet
                    char ssid[33];
                    char passwd[129];
                    
                    // split string
                    // ssid, starts at buffer+2, search for marker
                    unsigned int i;
                    int marker_found = 0;
                    for (i=0; i<size-2; i++) {
                        if (buffer[i+2]!='\n') {
                            ssid[i] = buffer[i+2];
                        } else {
                            marker_found = 1;
                            ssid[i] = '\0';
                            break;
                        }
                    }
                    // found marker?
                    if (!marker_found) {
                        // error
                        uint8_t stat_err_packet[3] = { PKT_STAT, STAT_ERR, ERR_PARAMS };
                        ioPacketSerial.send(stat_err_packet, 3);
                        break;
                    }

                    // password, starts at buffer+2+i+1, append null byte
                    unsigned int j;
                    for (j=0; j<size-2-i-1; j++) {
                        passwd[j] = buffer[j+2+i+1];
                    }
                    passwd[j] = '\0';

                    // try to connect
                    uint8_t ip[4];
                    int connected = wifi_connect_password(ssid, passwd, ip);
                    if (connected == EXIT_SUCCESS) {
                        // ok, connected
                        uint8_t data_conn_packet[7] = { PKT_DATA, DATA_CONN, IPV4, ip[0], ip[1], ip[2], ip[3] };
                        ioPacketSerial.send(data_conn_packet, 7);
                        //WiFi.disconnect();
                    } else {
                        // error
                        uint8_t stat_err_packet[3] = { PKT_STAT, STAT_ERR, ERR_CONN };
                        ioPacketSerial.send(stat_err_packet, 3);
                    }
                } else {
                    // error
                    uint8_t stat_err_packet[3] = { PKT_STAT, STAT_ERR, ERR_PARAMS };
                    ioPacketSerial.send(stat_err_packet, 3);
                }    
                break;
            case CMD_WIFI_CHG_MAC:
                // we need 6 parameters (mac)
                if (size > 7) {
                    // change mac
                    uint8_t mac[6] = {buffer[2], buffer[3], buffer[4], buffer[5], buffer[6], buffer[7]};
                    int result = wifi_change_mac(mac);
                    if (result == EXIT_SUCCESS) {
                        ioPacketSerial.send(stat_ok_packet, 2);
                    } else {
                        uint8_t stat_err_packet[3] = { PKT_STAT, STAT_ERR, ERR_UNKN };
                        ioPacketSerial.send(stat_err_packet, 3);
                    }
                } else {
                    // error
                    uint8_t stat_err_packet[3] = { PKT_STAT, STAT_ERR, ERR_PARAMS };
                    ioPacketSerial.send(stat_err_packet, 3);
                }    
                break;
            default:
                uint8_t stat_err_packet[3] = { PKT_STAT, STAT_ERR, ERR_WRONG };
                ioPacketSerial.send(stat_err_packet, 2);
                break;
        }

    }

    // parsed, blink
    board_blink();
}

//////////////// SETUP /////////////////
void setup() {
    // create init packet
    uint8_t stat_init_packet[2] = { PKT_STAT, STAT_INIT };

    // set buffer memory
    memset(ssids_packet, 0, BUFFER_SIZE);

    // internal led is inverted logic
    pinMode(INTERNAL_LED, OUTPUT);
    digitalWrite(INTERNAL_LED, HIGH);

    // init packetSerial using default Serial port
    // and set up callback, then send init packet
    ioPacketSerial.begin(SERIAL_SPEED);
    ioPacketSerial.setPacketHandler(&on_packet_received);
    ioPacketSerial.send(stat_init_packet, 2);
    // This initializes the WS2812FX library.
    pixels.init();
    pixels.setMode(FX_MODE_STATIC);
    pixels.setColor(IUVIA_COLOR_R, IUVIA_COLOR_G, IUVIA_COLOR_B);
    pixels.setSpeed(4096);
    pixels.setBrightness(255);
    pixels.start();

    // splash
    pixels.setMode(FX_MODE_FADE);

    // start wifi and disconnect
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();

    // alive
    board_blink();
}


//////////////// LOOP //////////////////

void loop() {
    // parse serial data
    ioPacketSerial.update();
    // animate
    pixels.service();
    // feed watchdog
    yield();
}
