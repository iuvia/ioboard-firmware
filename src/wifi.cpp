// (C) 2018 David Pello Gonzalez for IUVIA.io
// WiFi related functions

#include "wifi.h"

// scan wifi networks and store first n SSIDs in buffer, 
// returns stored buffer length or 0 on failure
int wifi_scan(int n, char* buffer, unsigned int buflen) {
    // we need to scan for at least one network
    if (n <= 0) {
        return PARAM_ERROR;
    }
                                                            
    // check buflen
    if (buflen <= 0) {
        return PARAM_ERROR;
    }

    // scan
    int count = WiFi.scanNetworks();

    // got WiFi?
    if (count == 0) {
        return 0;
    }

    // got more networks than asked for?
    if (count > n) {
        count = n;
    }

    // position and check buffer length
    unsigned int bufcheck = 0;

    // get SSIDs
    for (int i = 0; i < count; i++)
    {
        // copy first SSID
        if (i == 0) {
            // buffer big enough? (+2 bytes enc + rssi)
            if (bufcheck + WiFi.SSID(i).length() + 2 >= buflen) {
                // buffer full
                return BUFFER_ERROR;
            }
            // ok? copy
            memcpy(buffer, WiFi.SSID(i).c_str(), WiFi.SSID(i).length());
            bufcheck += WiFi.SSID(i).length();
            // add wifi info
            buffer[bufcheck] = WiFi.encryptionType(i);
            buffer[bufcheck+1] = (char) (WiFi.RSSI(i) < 0 ? -1 * WiFi.RSSI(i): WiFi.RSSI(i));
            bufcheck += 2;
        } else { // append
            // add separator
            if (bufcheck + 1 >= buflen) {
                // buffer full
                return BUFFER_ERROR;
            }
            // ok? copy
            buffer[bufcheck] = SEPARATOR;
            bufcheck++;
            // and append next SSID
            if (bufcheck + WiFi.SSID(i).length() + 2 >= buflen) {
                // buffer full
                return BUFFER_ERROR;
            }
            // ok? copy
            memcpy(buffer + bufcheck, WiFi.SSID(i).c_str(), WiFi.SSID(i).length());
            bufcheck += WiFi.SSID(i).length();
            // add wifi info
            buffer[bufcheck] = WiFi.encryptionType(i);
            buffer[bufcheck+1] = (char) (WiFi.RSSI(i) < 0 ? -1 * WiFi.RSSI(i): WiFi.RSSI(i));
            bufcheck += 2;
        }
    }  

    // clear memory
    WiFi.scanDelete();
    // everything ok?
    return bufcheck;

}

// try to connect to specified (open) wifi
int wifi_connect(char* ssid, uint8_t* ip) {
    
    // try to connect for 10 seconds
    WiFi.begin(ssid);
    for (int i=0; i<10; i++) {
        if (WiFi.status() == WL_CONNECTED) {
            for (int j=0; j<4; j++) {
                ip[j] = WiFi.localIP()[j];
            }
            return EXIT_SUCCESS;
        }
        delay(1000);
    }

    // not connected?
    return EXIT_FAILURE;
}

// try to connect to specified wifi with password
int wifi_connect_password(char* ssid, char* password, uint8_t* ip) {

    // try to connect for 10 seconds
    WiFi.begin(ssid, password);
    for (int i=0; i<10; i++) {
        if (WiFi.status() == WL_CONNECTED) {
            for (int j=0; j<4; j++) {
                ip[j] = WiFi.localIP()[j];
            }
            return EXIT_SUCCESS;
        }
        delay(1000);
    }

    // not connected?
    return EXIT_FAILURE;
}

// change the MAC address
int wifi_change_mac(uint8_t* mac) {
    // be sure it's disconnected
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    // change the mac
    // beware of multicast macs
    mac[0] = mac[0] & ~0x01;
    wifi_set_opmode(STATIONAP_MODE);
    wifi_set_macaddr(STATION_IF, mac);
    // Ok
    return EXIT_SUCCESS;
}
