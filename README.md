# IUVIA IO BOARD

This software controls the LED user interface in the IUVIA hardware, and other functions of the IO board like power measurement.

The LEDs are ws2811 "neopixels". This program uses the NeoPixelBus library to control them:
https://github.com/Makuna/NeoPixelBus

## Serial Protocol

In order to make the IO board compatible with several hardware platforms, a simple serial protocol is used in order to send commands and to receive information from it.

The serial protocol consists of variable lenght packets, using the PacketSerial library: https://github.com/bakercp/PacketSerial

The packet payload encodes several commands and information requests using simple 1-byte fields.

| 0           | 1                 | 2                   | 3                   | ... |
|-------------|-------------------|---------------------|---------------------|-----|
| Packet type | Command/Data type | 1st Parameter/field | 2nd Parameter/field | ... |


### Packet types and values:

* PKT_CMD - 0xCC: Send a command to the IO board.
* PKT_STAT - 0x57: Status packet. If status == error, 3rd byte will be error type
* PKT_DATA - 0xDA: Data packet.

### Command types:

#### LED Commands:
All LED commands return a STATUS OK packet if successful.

* CMD_LED_DELAY - 0x1D: Changes the delay of the animations (more delay is slower), 16 bits. 2 params, delay = p1 << 8 + p2
* CMD_LED_CLR - 0x1C: Clear all LEDs. 
* CMD_LED_FILL - 0x1F: Fill all LEDs with color. Three params, R, G and B.
* CMD_LED_FADEIN - 0x12: Fade from black to color, can use three params for R, G and B, if omitted default color is used.
* CMD_LED_FADEOUT - 0x13: Fade from color to black, can use three params for R, G and B, if omitted default color is used.
* CMD_LED_BREATH - 0x1B: Breath animation, can use three params more for R, G and B, if omitted default color is used.
* CMD_LED_RAINBW - 0x19: Rainbow animation.
* CMD_LED_IUVIA - 0x11: Iuvia animation, can use three params more for R, G and B, if omitted default color is used.
* CMD_LED_TRAIL - 0x17: Trailing animation, can use three params more for R, G and B, if omitted default color is used.
* CMD_LED_ERROR - 0x1E: Error - All LEDs red.

#### WiFi Commands:

* CMD_WIFI_SCAN - 0x35: Ask IOBOARD to scan wifi networks. One param with the number of SSID to return. Returns a DATA_SSIDS packet with SSIDs separated by SEPARATOR (defined in iuviaioboard.h). Each SSID last two bytes are not part of the SSID itself but represent encryption type (see encryption types), and RSSI (- n dBm).
* CMD_WIFI_CONN - 0x3C: Try to connect to the specified open SSID (one param - multibyte). Returns DATA_CONN packet if connection suceeds, STATUS ERR if not.
* CMD_WIFI_CONN_PSK - 0x3C: Try to connect to the specified SSID/PASSWORD (one param - multibyte, SSID and password must be sent with SEPARATOR between them). Returns DATA_CONN packet if connection suceeds, STATUS ERR if not.
* CMD_WIFI_CHG_MAC - 0x33: Change the MAC address of the device. Params: 6 bytes with the new MAC adress.

#### Other Commands:

* CMD_PWR_GET - 0x96: Get power measurement. Returns a DATA_PWR packet with the measurement.

### Data types:

* DATA_PWR - 0xD9: Measured power. 
* DATA_SSIDS - 0xD5: List of scanned SSIDs, separated by SEPARATOR (iuviaioboard.h).
* DATA_CONN - 0xDC: Connection info. 1 byte representing IPv4 or IPv6 and N bytes representing the IP (4 or 16 bytes).
* DATA_DEBUG - 0xDD: Debug purposes.

### Status messages:
* STAT_OK - 0x14
* STAT_ERR - 0xEE
* STAT_INIT - 0x17

### Status errors:

* ERR_LEDS - 0xE1: problem with LEDs
* ERR_PARAMS - 0xE2: wrong function parameters
* ERR_BUFFER - 0xE3: buffer size exceeded
* ERR_SCAN - 0xE5: problem scanning WiFis
* ERR_CONN - 0xE6: problem connecting to WiFi
* ERR_WRONG - 0xEE: wrong command
* ERR_UNKN - 0xEF: unknown error

### Encryption types

* WEP - 0x05
* WPA / PSK (TKIP) - 0x02
* WPA2 / PSK (CCMP) - 0x04
* OPEN - 0x07

## Linux side

Packets can be encoded/decoded using COBS encoding with the python COBS library.

See [test/](test/) folder for an example of actual python code for IOBOARD interfacing.

For example, to ask the IOBOARD for a LED breath animation, with a (100, 10, 1) RGB color, we need to send:
```
    PKT_CMD + CMD_LED_BREATH + 100 + 10 + 1
```
So the packet will be formed with the bytes:
```
    0xCC 0x1B 0x64 0x0A 0x01
```

After the COBS encoding (no zero bytes found in this packet), the complete packet will be:
```
    0x06 0xCC 0x1B 0x64 0x0A 0x01 0x00
```

the first byte 0x06 is where the zero byte is present in the packet, so the final 0x00 is the packet marker

Then the IOBOARD will answer with the following:
```
    0x03 0x57 0x14 0x00
```
after removing the packet marker (0x00) and the zero byte marker (0x03) we have a STATUS (0x57) packet with a STATUS OK payload (0x14)

## Python interface

A python interface is available in the [test/](test/) folder, *iuviaioboard.py*. This contains low and high-level interfaces for communicating with the IOBOARD. Main classes are:

* **IuviaIOPacket**: Creates and does all the encoding/decoding of message packets needed to interface with the IOBOARD. Contains all the constants related to commands, status messages and the like. Main methods are:
    * Constructor(packet type: int, content type: int, params[]): Creates a packet of type packet and content type, with a list of optional parameters (can be empty)
    * @from_bytes(data: bytearray): Creates a packet from a bytearray containing correct COBS encoded data (as received from the IOBOARD over the serial line)
    * encode(): Creates the COBS encoded bytearray dor sending over the serial line.
    * error_type(): If packet is an status packet containing a status error type, get the error type.
    * error_str(): Get the error as a string describing it.
    * data_type(): If packet is a data packet, get the data type.
    * data_str(): Get the data type as a string describing it.
    * @string_to_bytes(): creates a byte list from a string.


* **IuviaWifiNetwork**: Helps dealing with WiFi related messages, scan lists, connections, etc. 
    * Constructor(name: bytearray, enc: int, rssi: int): Creates a wifi network object, described by its name (SSID), encryption type and rssi
    * @create_wifi_list(data: bytearray): Creates a list containing WiFi description objects from a bytearray that contains the answer of a wifi scan command.
    * @create_wifi_credentials(ssid:str, password: str): Creates the encoded credentials for a connect command in the format expected by the IOBOARD.
    * @decode_ip(data: bytearray): Creates an IP (as a list of integers) from the data answered by a successful connect command.
    * @ip(data: bytearray): Same as decode_ip() but returns a string containing the IP.


* **IuviaIOBoard**: High-level interface that does all the encoding and serial port messaging. All methods but the constructor return a tuple consisting of a boolean telling if the action was successful or not, and a list with the answer if there is any.
    * Constructor(serial_port: str): Opens and configures the serial port connection with the IOBOARD.
    * animation(animation: int, params[]): Asks the IOBOARD for a LED animation of type "animation" with the optional params list. Animation types (for parameters see [Command types](#command-types)):
        * ANIMATION_CLR
        * ANIMATION_FILL
        * ANIMATION_FADEIN
        * ANIMATION_FADEOUT
        * ANIMATION_BREATH
        * ANIMATION_RAINBOW
        * ANIMATION_IUVIA
        * ANIMATION_TRAIL
        * ANIMATION_ERROR 
    * animation_delay(delay: int): Controls the speed of the animations changing the delay between frames. Delay is a 16bit integer.
    * wifi_scan(limit: int): Runs a scan for wifi networks with a maximum of "limit" networks returned. The list of the returned tuple contains IuviaWifiNetwork objects.
    * wifi_connect(ssid: str, password: str): Tries to connect to the specified wifi network, password can be ommited if the network is open. The tuple returned will have a list containing the IP if successful.
    * wifi_change_mac(mac: list): Changes the MAC address of the IOBOARD. The parameter is a list with the 6 fields of the MAC address.


