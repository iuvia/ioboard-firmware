import serial
import struct
from typing import Tuple
from cobs import cobs

ANIMATION_CLR = 0x1C
ANIMATION_FILL = 0x1F
ANIMATION_FADEIN = 0x12
ANIMATION_FADEOUT = 0x13
ANIMATION_BREATH = 0x1B
ANIMATION_RAINBOW = 0x19
ANIMATION_IUVIA = 0x11
ANIMATION_TRAIL = 0x17
ANIMATION_ERROR = 0x1E

# classes
class IuviaIOPacket:
    # class constants
    PACKET_MARKER = b'\x00'     # COBS 1-byte delimiter is hex zero as a (binary) bytes character
    SEPARATOR = b'\n'           # separator between multibyte packet fields 

    # packet types
    PKT_CMD = 0xCC              # Send a command to the IO board
    PKT_STAT = 0x57             # Status packet
    PKT_DATA = 0xDA             # Data packet

    # commands
    CMD_LED_DELAY = 0x1D        # Change the delay of the animation. 16bit. Two params, delay = P1 << 8 + P2
    CMD_LED_CLR = 0x1C          # Clear all LEDs
    CMD_LED_FILL = 0x1F         # Fill all LEDs with color. Three params, R, G, B.
    CMD_LED_FADEIN = 0x12       # Fade from black to color, min. 1 param, delay between frames. Can use three params more for R, G and B.
    CMD_LED_FADEOUT = 0x13      # Fade from color to black, min. 1 param, delay between frames. Can use three params more for R, G and B.
    CMD_LED_BREATH = 0x1B       # Breath animation, min. two params, delay between frames and repeats. Can use three params more for R, G and B.
    CMD_LED_RAINBW = 0x19       # Rainbow animation, two params, delay between frames and repeats
    CMD_LED_IUVIA = 0x11        # Iuvia animation, min. two params, delay between frames and repeats. Can use three params more for R, G and B.
    CMD_LED_TRAIL = 0x17        # Trailing animation, min. two params, delay between frames and repeats. Can use three params more for R, G and B.
    CMD_LED_ERROR = 0x1E        # Error - All LEDs red
    CMD_PWR_GET = 0x99          # Get power measurement
    CMD_WIFI_SCAN = 0x35        # Ask IOBOARD to scan wifi networks.
    CMD_WIFI_CONN = 0x3C        # Try to connect to the specified open SSID (one param - multibyte).
    CMD_WIFI_CONN_PSK = 0x39    # Try to connect to the specified SSID + SEPARATOR + PASSWORD (one param - multibyte).
    CMD_WIFI_CHG_MAC = 0x33     # Change the MAC address of the device. Params: 6 bytes with the new MAC adress.

    # data types
    DATA_DEBUG = 0xDD       # Debug data (string)
    DATA_PWR = 0xD9         # Measured power
    DATA_SSIDS = 0xD5       # List of scanned SSIDs, comma separated
    DATA_CONN = 0xDC        # IP of active connection

    # status 
    STAT_OK = 0x14
    STAT_ERR = 0xEE
    STAT_INIT = 0x17

    # errors
    ERR_LEDS = 0xE1     # problem with LEDs
    ERR_PARAMS = 0xE2   # wrong function parameters
    ERR_BUFFER = 0xE3   # buffer size exceeded
    ERR_SCAN = 0xE5     # problem scanning WiFis
    ERR_CONN = 0xE6     # problem connecting to WiFi
    ERR_WRONG = 0xEE    # wrong command
    ERR_UNKN = 0xEF     # unknown error

    # dictionaries
    packets = {
            PKT_CMD: "COMMAND PACKET",
            PKT_DATA: "DATA PACKET",
            PKT_STAT: "STATUS PACKET",
    }

    commands = {
            CMD_LED_DELAY: "LED DELAY",
            CMD_LED_CLR: "LED CLEAR",
            CMD_LED_FILL: "LED FILL",
            CMD_LED_RAINBW: "LED RAINBOW",
            CMD_LED_BREATH: "LED BREATH",
            CMD_LED_IUVIA: "LED IUVIA",
            CMD_LED_ERROR: "LED ERROR",
            CMD_LED_TRAIL: "LED TRAIL",
            CMD_PWR_GET: "GET POWER",
            CMD_WIFI_SCAN: "WIFI SCAN",
            CMD_WIFI_CONN: "WIFI CONNECT",
            CMD_WIFI_CONN_PSK: "WIFI CONNECT PASSWORD",
            CMD_WIFI_CHG_MAC: "WIFI CHANGE MAC",
    }

    data = {
            DATA_DEBUG: "DEBUG DATA",
            DATA_PWR: "POWER DATA",
            DATA_SSIDS: "SSID LIST",
            DATA_CONN: "CONN DATA",
    }
    
    status = {
            STAT_OK: "STATUS OK",
            STAT_ERR: "STATUS ERROR",
            STAT_INIT: "STATUS INIT",
    }
    
    errors = {
            ERR_LEDS: "ERROR LEDS",
            ERR_PARAMS: "ERROR PARAMS",
            ERR_BUFFER: "ERROR BUFFER",
            ERR_SCAN: "ERROR SCANNING",
            ERR_CONN: "ERROR CONNECTING",
            ERR_WRONG: "ERROR COMMAND",
            ERR_UNKN: "ERROR UNKNOWN",
    }
    
    # create packet from fields
    def __init__(self, packet_type: int, content_type: int, payload: list = []):
        self.packet_type = packet_type
        self.content_type = content_type
        self.payload = payload

    # create packet from COBS-encoded bytes
    @classmethod
    def from_bytes(cls, data: bytearray):
        data = cobs.decode(data.rstrip(cls.PACKET_MARKER))
        packet = cls(data[0], data[1], data[2:])
        return packet

    # create COBS-encoded bytes for seding over serial
    def encode(self):
        data = bytearray(2+len(self.payload))
        data[0] = self.packet_type
        data[1] = self.content_type
        n = 2
        for b in self.payload:
            data[n] = b
            n = n+1

        pak = cobs.encode(data)
        pak = pak + self.PACKET_MARKER
        return pak

    def __str__(self):
        return self.encode().hex()

    # getters
    def error_type(self):
        if self.packet_type == self.PKT_STAT and self.content_type == self.STAT_ERR:
            return self.payload[0]
        else:
            return None
        
    def error_str(self):
        if self.packet_type == self.PKT_STAT and self.content_type == self.STAT_ERR:
            return self.errors[self.payload[0]]
        else:
            return None
        
    def data_type(self):
        if self.packet_type == self.PKT_DATA:
            return self.content_type
        else:
            return None
        
    def data_str(self):
        if self.packet_type == self.PKT_DATA:
            return self.data[self.content_type]
        else:
            return None
        
    # creates a byte list from a string
    @staticmethod
    def string_to_bytes(s: str):
        return [ord(char) for char in s]
    

class IuviaWifiNetwork:
    # constants
    IPV4 = 0x04
    IPV6 = 0x06
    wifi_types = {0x02: "WPA", 0x04: "WPA2", 0x05: "WEP", 0x07: "OPEN", 0x08: "WPA/WPA2", 0xFF: "Unknown"}
    ip_types = {0x04: "IPV4", 0x06: "IPV6"}
    
    def __init__(self, name: bytearray, enc: int, rssi: int):
        self.name = str(name, "utf-8")
        self.enc = enc
        self.rssi = -rssi
    def __str__(self):
        return self.name + " " + self.wifi_types[self.enc] + " " + str(self.rssi) + " dBm"

    # creates a wifi list from data
    @staticmethod
    def create_wifi_list(data: bytearray):
        # split data in a list
        list = data.split(IuviaIOPacket.SEPARATOR)
        networks = []
        for net in list:
            network = IuviaWifiNetwork(net[:-2],
                int.from_bytes(net[-2:-1], byteorder='big'),
                int.from_bytes(net[-1:], byteorder='big'))
            networks.append(network)
        return networks
    
    # creates bytearray with wifi credentials
    @staticmethod
    def create_wifi_credentials(ssid: str, password: str = None):
        data = ssid + str(IuviaIOPacket.SEPARATOR, "ascii") + password if password != None else ""
        return [ord(char) for char in data] 
        
    # creates an IP from data
    @staticmethod
    def decode_ip(data: bytearray):
        if data[0] == IuviaWifiNetwork.IPV4:
            return (data[0], (data[1], data[2], data[3], data[4]))
            
    @staticmethod
    def ip(data: bytearray):
        if data[0] == IuviaWifiNetwork.IPV4:
            return "{}.{}.{}.{}".format(data[1], data[2], data[3], data[4]) 

class IuviaIOBoard:
    def __init__(self, serial_port: str):
        self.port = serial.Serial(serial_port, baudrate=115200, bytesize=8, parity='N', stopbits=1, timeout=15)

    def animation(self, animation: int, params: list = []) -> Tuple[bool, list]:
        packet = IuviaIOPacket(IuviaIOPacket.PKT_CMD, animation, params)
        self.port.write(packet.encode())

        # read answer
        answer = self.port.read_until(IuviaIOPacket.PACKET_MARKER)
        decoded = IuviaIOPacket.from_bytes(answer)
        if decoded.content_type == IuviaIOPacket.STAT_ERR:
            return (False, [])
        else:
            return (True, [])
    
    def animation_delay(self, speed: int) -> Tuple[bool, list]:
        # check size 
        if speed > 2**16 or speed < 1:
            return (False, [])

        speed_high = (speed >> 8) & 0xff
        speed_low = speed & 0xff

        packet = IuviaIOPacket(IuviaIOPacket.PKT_CMD, IuviaIOPacket.CMD_LED_DELAY, [speed_high, speed_low])
        self.port.write(packet.encode())

        # read answer
        answer = self.port.read_until(IuviaIOPacket.PACKET_MARKER)
        decoded = IuviaIOPacket.from_bytes(answer)
        if decoded.content_type == IuviaIOPacket.STAT_ERR:
            return (False, [])
        else:
            return (True, [])


    def wifi_scan(self, limit: int = 32) -> Tuple[bool, list]:
        packet = IuviaIOPacket(IuviaIOPacket.PKT_CMD, IuviaIOPacket.CMD_WIFI_SCAN, [limit])
        self.port.write(packet.encode())

        # read answer
        answer = self.port.read_until(IuviaIOPacket.PACKET_MARKER)
        decoded = IuviaIOPacket.from_bytes(answer)
        if decoded.packet_type == IuviaIOPacket.PKT_DATA:
            wifis = IuviaWifiNetwork.create_wifi_list(decoded.payload)
            return (True, wifis)
        else:
            if decoded.content_type == IuviaIOPacket.STAT_ERR:
                return (False, [])

    def wifi_connect(self, ssid: str, password: str = None) -> Tuple[bool, list]:
        packet_type = 0
        if password == None:
            packet_type = IuviaIOPacket.CMD_WIFI_CONN
        else:
            packet_type = IuviaIOPacket.CMD_WIFI_CONN_PSK    
        packet = IuviaIOPacket(IuviaIOPacket.PKT_CMD, packet_type, IuviaWifiNetwork.create_wifi_credentials(ssid, password))
        
        self.port.write(packet.encode())

        # read answer
        answer = self.port.read_until(IuviaIOPacket.PACKET_MARKER)
        decoded = IuviaIOPacket.from_bytes(answer)
        if decoded.packet_type == IuviaIOPacket.PKT_DATA:
            ip = IuviaWifiNetwork.ip(decoded.payload)
            return (True, [ip])
        else:
            if decoded.content_type == IuviaIOPacket.STAT_ERR:
                return (False, [])

    
    def wifi_change_mac(self, mac: list = [66,66,66,66,66,66]) -> Tuple[bool, list]:
        packet = IuviaIOPacket(IuviaIOPacket.PKT_CMD, IuviaIOPacket.CMD_WIFI_CHG_MAC, mac)
        self.port.write(packet.encode())

        # read answer
        answer = self.port.read_until(IuviaIOPacket.PACKET_MARKER)
        decoded = IuviaIOPacket.from_bytes(answer)
        if decoded.content_type == IuviaIOPacket.STAT_OK:
            return (True, [])
        else:
            return (False, [])

