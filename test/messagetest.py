import serial
from iuviaioboard import *

SERIAL_PORT = "/dev/ttyUSB0"

# SEND SOME PACKETS
ser = serial.Serial(SERIAL_PORT, baudrate=115200, bytesize=8, parity='N', stopbits=1, timeout=15)

# LED COMMAND 
#packet = IuviaIOPacket(IuviaIOPacket.PKT_CMD, IuviaIOPacket.CMD_LED_FADEOUT, [10, 200, 200, 40])
#packet = IuviaIOPacket(IuviaIOPacket.PKT_CMD, IuviaIOPacket.CMD_LED_RAINBW, [10, 100])
packet = IuviaIOPacket(IuviaIOPacket.PKT_CMD, IuviaIOPacket.CMD_LED_IUVIA, [100, 10, 0])

# debug
print ("SENT IUVIA ANIMATION: " + str(packet))

# send packet
ser.write(packet.encode())

# read answer
answer = ser.read_until(IuviaIOPacket.PACKET_MARKER)

print ("RECEIVED: " + answer.hex())
decoded = IuviaIOPacket.from_bytes(answer)
print (IuviaIOPacket.packets[decoded.packet_type], end=" : ")
print (IuviaIOPacket.status[decoded.content_type])
if decoded.content_type == IuviaIOPacket.STAT_ERR:
    print (decoded.error_str())

# SCAN WIFIS
packet = IuviaIOPacket(IuviaIOPacket.PKT_CMD, IuviaIOPacket.CMD_WIFI_SCAN, [32])

# debug
print ("SENT WIFI SCAN: " + str(packet))

# send packet
ser.write(packet.encode())

# read answer
answer = ser.read_until(IuviaIOPacket.PACKET_MARKER)
print ("RECEIVED: " + answer.hex())

decoded = IuviaIOPacket.from_bytes(answer)
print (IuviaIOPacket.packets[decoded.packet_type], end=" : ")
if decoded.packet_type == IuviaIOPacket.PKT_DATA:
    print (decoded.data_str())
    wifis = IuviaWifiNetwork.create_wifi_list(decoded.payload)
    for wifi in wifis:
        print(wifi)
else:
    print (IuviaIOPacket.status[decoded.content_type])
    if decoded.content_type == IuviaIOPacket.STAT_ERR:
        print (decoded.error_str())

# CHANGE MAC COMMAND
packet = IuviaIOPacket(IuviaIOPacket.PKT_CMD, IuviaIOPacket.CMD_WIFI_CHG_MAC, [0x66, 0x66, 0x66, 0x66, 0x66, 0x66])

#debug
print ("SENT IUVIA CHANGE MAC: " + str(packet))

#send packet
ser.write(packet.encode())

#read answer
answer = ser.read_until(IuviaIOPacket.PACKET_MARKER)

print ("RECEIVED: " + answer.hex())
decoded = IuviaIOPacket.from_bytes(answer)
print (IuviaIOPacket.packets[decoded.packet_type], end=" : ")
print (IuviaIOPacket.status[decoded.content_type])
if decoded.content_type == IuviaIOPacket.STAT_ERR:
    print (decoded.error_str())


# CONNECT COMMAND 
packet = IuviaIOPacket(IuviaIOPacket.PKT_CMD, IuviaIOPacket.CMD_WIFI_CONN_PSK, IuviaWifiNetwork.create_wifi_credentials("USER", "PASSWORD"))

# debug
print ("SENT WIFI CONNECT: " + str(packet))

# send packet
ser.write(packet.encode())

# read answer
answer = ser.read_until(IuviaIOPacket.PACKET_MARKER)

print ("RECEIVED: " + answer.hex())
decoded = IuviaIOPacket.from_bytes(answer)
print (IuviaIOPacket.packets[decoded.packet_type], end=" : ")
if decoded.packet_type == IuviaIOPacket.PKT_DATA:
    print (decoded.data_str())
    ip = IuviaWifiNetwork.ip(decoded.payload)
    print(ip)
else:
    print (IuviaIOPacket.status[decoded.content_type])
    if decoded.content_type == IuviaIOPacket.STAT_ERR:
        print (decoded.error_str())
