from iuviaioboard import *

ioboard = IuviaIOBoard("/dev/ttyUSB0")

result = ioboard.animation_delay(1500)
result = ioboard.animation(ANIMATION_TRAIL, [55, 0, 0])
print(result)
ioboard.animation(ANIMATION_CLR, [])

result = ioboard.wifi_scan()
if result[0] != False:
    for network in result[1]:
        print(network)

result = ioboard.wifi_change_mac()

result = ioboard.wifi_connect("SSID", "pass")
if result[0] != False:
    print (result[1])

